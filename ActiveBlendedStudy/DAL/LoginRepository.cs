﻿using BOL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class LoginRepository : DashboardRepository
    {
       

        

        /// <summary>
        /// Get all the users
        /// </summary>
        /// <returns>The list of all users present in the dB</returns>
        public IEnumerable<User> GetAllUsers()
        {
            return dB.Users;
        }

        /// <summary>
        /// Get user by his user ID
        /// </summary>
        /// <param name="userId"> The user Id attached to the user.</param>
        /// <returns>The user object if found, else null</returns>
        public User GetUser(int userId)
        {
            try
            {
                return dB.Users.Single(u => u.User_ID == userId);
            }
            catch(Exception e)
            {
                Console.WriteLine("Error occured: " + e.ToString());
                return null;
            }
            
        }
    }
}
